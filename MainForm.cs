﻿using System;
using System.Windows.Forms;

namespace Miner
{
    public delegate void SetFormStatusDelegate();

    public delegate void SetFlagsCountDelegate(int value);

    public delegate void MineFieldDelegate(MineField mine);


    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void SetMineField(MineField field)
        {
            minesFieldPanel.Controls.Add(field);
        }

        private void SetFlagsCount(int count)
        {
            this.flagsCountLabel.Text = "Flags: " + count;
        }

        private void ChangeFormType(int fieldWidth)
        {
            this.Height = 130 + fieldWidth * 30;
            this.Width = 60 + fieldWidth * 30;
            fieldSizeLabel.Text = "Mines: ";
            minesCountField.Visible = false;
            playButton.Visible = false;
            flagsCountLabel.Text = "Flags: 0";
            flagsCountLabel.Visible = true;
        }

        public void ChangeFormType()
        {
            this.Height = 160;
            this.Width = 320;
            fieldSizeLabel.Text = "Field size: ";
            minesCountField.Visible = true;
            playButton.Visible = true;
            flagsCountLabel.Visible = false;
        }

        private void newGameItem_Click(object sender, EventArgs e)
        {
            GameLogic.EndGame();
            this.ChangeFormType();
        }

        private void exitItem_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(0);
        }

        private void aboutItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Classic miner.\nLeft mouse to check the field\nRight mouse to put a flag on the field\nDeveloper: El Lobo-Aviador");
        }

        private void playButton_Click(object sender, EventArgs e)
        {
            try
            {
                int minesCount = Convert.ToInt32(this.minesCountField.Text);
                this.ChangeFormType(minesCount);
                if ((minesCount > 3) && (minesCount < 26))
                {
                    fieldSizeLabel.Text += GameLogic.CreateFields
                    (
                        minesCount,
                        this.SetFlagsCount,
                        this.ChangeFormType,
                        this.SetMineField
                    );
                }
                else
                {
                    throw new Exception("Enter the number between 3 and 25");
                }
            }
            catch (FormatException)
            {
                MessageBox.Show("Enter integer number");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unknown error: " + ex.Message);
            }
        }
    }
}
