﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Miner
{
    public class MineField : Button
    {
        public bool IsMined { get; set; }
        public bool IsChecked { get; set; }
        public int AroundMinesCount { get; set; }
        public int Idx { get; private set; }
        public int Idy { get; private set; }
        private bool IsFlagged { get; set; }

        public MineField(int idx, int idy, int x, int y)
        {
            this.Height = 30;
            this.Width = 30;
            this.Idx = idx;
            this.Idy = idy;
            this.Text = "";
            Location = new Point(x, y);
            IsMined = false;
            IsChecked = false;
            IsFlagged = false;
            this.Click += MineField_Click;
            this.MouseDown += MineField_MouseDown;
            this.Font = new Font("Arial", 9F, FontStyle.Bold);
        }

        private void CheckColor()
        {
            switch (this.AroundMinesCount)
            {
                case 0:
                    this.ForeColor = Color.Gray;
                    break;
                case 1:
                    this.ForeColor = Color.Black;
                    break;
                case 2:
                    this.ForeColor = Color.Blue;
                    break;
                case 3:
                    this.ForeColor = Color.Green;
                    break;
                case 4:
                    this.ForeColor = Color.Orange;
                    break;
                default:
                    this.ForeColor = Color.White;
                    break;
            }
        }

        public void ShowAroundMinesCount()
        {
            this.CheckColor();
            this.Text = AroundMinesCount.ToString();
        }

        public void ShowMine()
        {
            if (IsMined == true)
            {
                this.ForeColor = Color.Red;
                this.Text = "X";
            }
        }

        private void MineField_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (IsChecked == false)
                {
                    if (IsFlagged == false)
                    {
                        this.ForeColor = Color.Orange;
                        this.Text = "<|";
                        IsFlagged = true;
                        GameLogic.SetFlag(IsFlagged); // Вынести общие данные в какой-то отдельный класс
                    }
                    else
                    {
                        this.Text = "";
                        IsFlagged = false;
                        GameLogic.SetFlag(IsFlagged);
                    }
                }
            }
        }

        private void MineField_Click(object sender, EventArgs e)
        {
            if (IsFlagged == true)
            {
                IsFlagged = false;
                GameLogic.SetFlag(IsFlagged);
            }

            GameLogic.CheckField(this);
        }

        public void Destroy()
        {
            this.Dispose();
        }

    }    
}
