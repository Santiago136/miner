﻿using System.Windows.Forms;

namespace Miner
{
    partial class MainForm
    {
        private System.ComponentModel.IContainer components = null;

        Panel infoPanel;
        Panel minesFieldPanel;
        Label fieldSizeLabel;
        Label flagsCountLabel;
        TextBox minesCountField;
        Button playButton;
        MenuStrip menu;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Text = "Miner";
            this.Height = 160;
            this.Width = 320;

            minesFieldPanel = new Panel();
            minesFieldPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            minesFieldPanel.Dock = DockStyle.Fill;
            this.Controls.Add(minesFieldPanel);

            infoPanel = new Panel();
            infoPanel.Size = new System.Drawing.Size(this.Width, 30);
            infoPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D; ;
            infoPanel.Dock = DockStyle.Top;
            this.Controls.Add(infoPanel);

            fieldSizeLabel = new Label();
            fieldSizeLabel.Location = new System.Drawing.Point(20, 5);
            fieldSizeLabel.AutoSize = true;
            fieldSizeLabel.Text = "Field size: ";
            infoPanel.Controls.Add(fieldSizeLabel);

            minesCountField = new TextBox();
            minesCountField.Location = new System.Drawing.Point(110, 3);
            minesCountField.Width = 60;
            minesCountField.Text = "10";
            infoPanel.Controls.Add(minesCountField);

            flagsCountLabel = new Label();
            flagsCountLabel.Location = new System.Drawing.Point(90, 5);
            flagsCountLabel.AutoSize = true;
            flagsCountLabel.Visible = false;
            flagsCountLabel.Text = "Flags: 0";
            infoPanel.Controls.Add(flagsCountLabel);

            playButton = new Button();
            playButton.Location = new System.Drawing.Point(60, 20);
            playButton.Text = "Play";
            playButton.BackColor = System.Drawing.Color.White;
            playButton.Click += playButton_Click;
            this.AcceptButton = this.playButton;
            minesFieldPanel.Controls.Add(playButton);

            menu = new MenuStrip();

            ToolStripMenuItem gameItem = new ToolStripMenuItem("Game");

            ToolStripMenuItem newGameItem = new ToolStripMenuItem("New");
            newGameItem.Click += newGameItem_Click;
            gameItem.DropDownItems.Add(newGameItem);

            ToolStripMenuItem exitItem = new ToolStripMenuItem("Exit");
            exitItem.Click += exitItem_Click;
            gameItem.DropDownItems.Add(exitItem);

            menu.Items.Add(gameItem);

            ToolStripMenuItem aboutItem = new ToolStripMenuItem("About");
            aboutItem.Click += aboutItem_Click;
            menu.Items.Add(aboutItem);

            this.Controls.Add(menu);
        }

        #endregion
    }
}

