﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Threading;

namespace Miner
{
    public static class GameLogic
    {
        static List<List<MineField>> mineFields;
        static int minesCount;
        static int clickCounter;
        static int width;
        static int flagsCount;
        static SetFlagsCountDelegate setFlagsCount;
        static SetFormStatusDelegate setFormStatus;
        static System.Threading.Timer timer;
        static int roundTime;

        private static void ComputeTime(object obj)
        {
            roundTime++;
        }

        public static int CreateFields
        (
            int fieldsCount,  //НЕ ПРАВИЛЬНО НАЗВАЛ
            SetFlagsCountDelegate setFlagsCount,
            SetFormStatusDelegate setFormStatus,
            MineFieldDelegate setMineFieldDel
        )
        {
            flagsCount = 0;
            roundTime = 0;
            timer = new System.Threading.Timer(ComputeTime, 0, 0, 1000);

            minesCount = fieldsCount + (fieldsCount / 10 * 3) + fieldsCount / 10;
            width = fieldsCount;
            GameLogic.setFlagsCount = setFlagsCount;
            GameLogic.setFormStatus = setFormStatus;

            mineFields = new List<List<MineField>>();
            clickCounter = 0;
            for (int i = 0; i < width; i++)
            {
                List<MineField> row = new List<MineField>();
                for (int j = 0; j < width; j++)
                {
                    MineField field = new MineField(i, j, 20 + 30 * j, 10 + 30 * i);
                    setMineFieldDel.Invoke(field);
                    row.Add(field);
                }
                mineFields.Add(row);
            }

            Random rnd = new Random();
            for (int i = 0; i < minesCount; i++)
            {
                int x = rnd.Next(mineFields.Count);
                int y = rnd.Next(mineFields.Count);
                while (mineFields[x][y].IsMined == true)
                {
                    x = rnd.Next(mineFields.Count);
                    y = rnd.Next(mineFields.Count);
                }
                mineFields[x][y].IsMined = true;
            }


            for (int k = 0; k < width; k++)
            {
                for (int l = 0; l < width; l++)
                {
                    if (mineFields[k][l].IsMined == false)
                    {
                        mineFields[k][l].AroundMinesCount = CountAroundMines(mineFields[k][l]);
                    }
                }
            }

            return minesCount;
        }

        private static int CountAroundMines(MineField field)
        {
            int mineCounter = 0;
            for (int i = field.Idx - 1; i <= field.Idx + 1; i++)
            {
                if (i >= 0 && i < width)
                {
                    for (int j = field.Idy - 1; j <= field.Idy + 1; j++)
                    {
                        if (j >= 0 && j < width)
                        {
                            if (mineFields[i][j].IsMined == true)
                            {
                                mineCounter++;
                            }
                        }
                    }
                }
            }
            return mineCounter;
        }

        public static void SetFlag(bool isFlagged)
        {
            if (isFlagged == true)
            {
                flagsCount++;
            }
            else
            {
                flagsCount--;
            }
            setFlagsCount.Invoke(flagsCount);
        }

        public static void CheckField(MineField field)
        {
            if (field.IsMined == true)
            {
                GameLogic.Loose(field);
            }
            else
            {
                if (field.IsChecked == false)
                {
                    clickCounter++;
                    field.IsChecked = true;

                    if (clickCounter == mineFields.Count * mineFields.Count - minesCount)
                    {
                        Win(field);
                    }
                    else
                    {
                        if (field.AroundMinesCount == 0) // условие выхода
                        {
                            for (int i = field.Idx - 1; i <= field.Idx + 1; i++)
                            {
                                if (i >= 0 && i < width)
                                {
                                    for (int j = field.Idy - 1; j <= field.Idy + 1; j++)
                                    {
                                        if (j >= 0 && j < width)
                                        {
                                            if (mineFields[i][j].IsChecked == false)
                                            {
                                                //CheckField(mineFields[i][j]);
                                                mineFields[i][j].PerformClick();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        field.ShowAroundMinesCount();
                    }
                }
            }
        }

        public static void EndGame()
        {
            if (mineFields != null)
            {
                for (int i = 0; i < mineFields.Count; i++)
                {
                    for (int j = 0; j < mineFields.Count; j++)
                    {

                        mineFields[i][j].Destroy();
                    }
                }
                setFormStatus.Invoke();
            }
        }

        private static void Win(MineField field)
        {
            timer.Dispose();
            MessageBox.Show("Вы выиграли! \nВремя игры: " + roundTime + " сек.");
            EndGame();
        }

        public static void Loose(MineField field)
        {
            timer.Dispose();
            for (int i = 0; i < mineFields.Count; i++)
            {
                for (int j = 0; j < mineFields.Count; j++)
                {
                    mineFields[i][j].ShowMine();
                }
            }
            MessageBox.Show("Вы проиграли! \nВремя игры: " + roundTime + " сек.");
            EndGame();
        }
    }
}